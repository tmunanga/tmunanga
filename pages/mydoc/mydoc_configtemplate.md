---
title: Configuration Guide Template
tags: [configuration]
keywords: configuration,template,configure,settings,setup
last_updated: December 8, 2018
summary: "This page explains how to use the configuration guide template."
sidebar: mydoc_sidebar
permalink: mydoc_configtemplate.html
folder: mydoc
---

# Configuration Guide Template

To use this template, you must modify the &lt;MapperName&gt; placeholder. In the code references, replace &lt;MapperName&gt; with the mapper name in lower case (for example, newmapper). In all other references, capitalize the mapper name as the proper noun (for example, New Mapper).

You must also review the following sections and update as needed so that it is specific to your mapper:

* **Add the &lt;MapperName&gt; Mapper to the** `docker-compose.yml` **File** - This will be provided by the CC Mapper team when the RC build is complete.
* **Copy the &lt;MapperName&gt; Mapper Resources Folder from Artifactory to the Environment** - This is provided by the team who builds the mapper. You need to update the list of artifacts that are included in the `resources.zip` archive file.
* **Update the Parameters in the `mapper-config.json` File** - This is provided by the team who builds the mapper. You need to update the code block to include the correct configuration parameters and default/placeholder values and the table to include the correct description of those configuration parameters.

Those are all the sections that need to be updated at this time. The others sections are static and can be used as is as long as no changes are made to the configuration process. That being said, your team needs to work closely with the IT/Ops team who deploy and configure your mappers to ensure that they have the information that they need to successfully deploy and configure your mapper.

## Assumptions 

Setting up the CrossCore environment and uploading the mapper is not covered in this document. This document will specifically outline the &lt;MapperName&gt; mapper and the configuration required for setting up a working call through the CrossCore environment. 

## Configuration to be Performed by the IT Team

The following steps are required to modify a CrossCore test environment to include the &lt;MapperName&gt; mapper.

<span style="background-color:rgb(255,253,246)">If configuring the mappers for UAT, remember that &lt;MapperName&gt; UAT is a customer-facing environment.</span>

&lt;MapperName&gt; mapper configuration requires updates to the following files:

<table style="width:100%">
<tr style="background-color:#DCDCDC;text-align:left;"><th>  File    </th><th>  Description      </th></tr>
<tr style="background-color:white;"><td>  docker-compose.yml      </td><td>  The docker compose file.      </td></tr>
<tr style="background-color:white;"><td>  mapper-config.json   </td><td>  JSON file containing mapper configuration key-value pairs.        </td></tr>
</table>

<span style="background-color:rgb(255,253,246)">You may see some scripts included in the mapper configuration artifact package. The scripts are used for internal testing. You must have root access to run the scripts successfully. Because most delivery and IT teams do not have root access permissions, the instructions on this page describe how to configure the mapper using the `mapper-config-cli` tool. These are the recommended instructions from the CrossCore team.</span>

The configuration of these files can be broken down into these steps:

1.	Add the &lt;MapperName&gt; mapper to the `docker-compose.yml` file.
2.	Copy the &lt;MapperName&gt; mapper Resources folder from Artifactory to the environment.
3.	Update the parameters in the `mapper-config.json` file.
4.	Seed the mapper configuration to ZooKeeper.
5.	Seed the backing application version to ZooKeeper.

The following process explains how to configure the files above and execute the mapper configuration.

### Add the &lt;MapperName&gt; Mapper to the `docker-compose.yml` File

<ol><li>To configure the &lt;MapperName&gt; mapper, edit the `docker-compose.yml` file with a text editor (for example, vi) and add the following &lt;MapperName&gt; mapper section (the values used are examples).<br />

<table style="border:0px;font-family:Consolas;font-size:9pt;"><tr><td>mapper-&lt;MapperName&gt;:<br />
  &nbsp;&nbsp;container_name: 'mapper-&lt;MapperName&gt;'<br />
  &nbsp;&nbsp;image: 'redbox-docker-release-local.eda-prod-hud03.uk.experian.local:2222/mapper-&lt;MapperName&gt;:&lt;version&gt;'<br />
  &nbsp;&nbsp;environment:<br />
    &nbsp;&nbsp;&nbsp;&nbsp;DW_ZOOKEEPER_CONNECT_STRING: 'zookeeper.experian.local:2181' # change to host:port of zookeeper<br />
    &nbsp;&nbsp;&nbsp;&nbsp;DW_ZOOKEEPER_KEY_STORE_PASSWORD: 'secret123' # change to password of keystore-zookeeper.jks<br />
    &nbsp;&nbsp;&nbsp;&nbsp;DW_DISCOVERY_HOST: 'mapper.experian.local' # change to hostname of server running this container<br />
    &nbsp;&nbsp;&nbsp;&nbsp;DW_DISCOVERY_PORT: '8443'<br />
  &nbsp;&nbsp;volumes:<br />
    &nbsp;&nbsp;&nbsp;&nbsp;- '/data/config/map/keystore-zookeeper.jks:/opt/crosscore/keys/keystore-zookeeper.jks'<br />
  &nbsp;&nbsp;ports:<br />
    &nbsp;&nbsp;&nbsp;&nbsp;- '&lt;PORT&gt;:8443'</td></tr>
</table></li>
<li>
Each mapper within Docker listens on port 8443 by default, but if several mappers are running on the Docker host they can't all be exposed on the same external port. You must expose a port on the mapper server for each mapper deployed. Configure each mapper to use different port and set the DW_DISCOVERY_HOST value and variable to match that port.</li>
<li>Save changes to the file.</li>
<li>Run the following command to install the &lt;MapperName&gt; mapper.<br />
    &nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:Consolas;font-size:9pt;">sudo docker-compose -f &lt;path to config file&gt;/docker-compose.yml up -d</span></li>
<li>Replace <span style="border:0px;font-family:Consolas;font-size:9pt;">docker-compose.yml</span> with the actual name of your YAML file.</li>
</ol>

### Copy the &lt;MapperName&gt; Mapper Resources Folder from Artifactory to the Environment

Each CrossCore backing application requires a number of configuration resources. These resources are stored in **Artifactory** and need to be copied to the Mapper server where the `mapper-config-cli` tool is installed. The &lt;MapperName&gt; mapper resources directory has the following structure.

> -mapper-config.json

<span style="background-color:rgb(255,253,246)">You may see some scripts included in the mapper configuration artifact package. The scripts are used for internal testing. You must have root access to run the scripts successfully. Because most delivery and IT teams do not have root access permissions, the instructions on this page describe how to configure the mapper using the `mapper-config-cli` tool. These are the recommended instructions from the CrossCore team.</span>
